class ChangeImageInProducts < ActiveRecord::Migration[7.0]
  def up
    add_column :products, :images, :json
    remove_column :products, :image
  end

  def down
    add_column :products, :image, :string
    remove_column :products, :images
  end
end

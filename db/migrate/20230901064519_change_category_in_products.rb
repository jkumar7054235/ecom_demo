class ChangeCategoryInProducts < ActiveRecord::Migration[7.0]
  def up
    remove_column :products, :category
    add_reference :products, :category, foreign_key: true
  end

  def down
    remove_reference :products, :category
    add_column :products, :category, :string
  end
end

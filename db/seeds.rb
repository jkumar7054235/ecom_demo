# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
if Rails.env.development?
  AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

  categories = RestClient.get("https://fakestoreapi.com/products/categories")
  JSON.parse(categories).each do |category|
    Category.create(name: category)
  end

  products = RestClient.get("https://fakestoreapi.com/products")

  JSON.parse(products).each do |product|
    Category.find_by_name(product["category"]).products.create do |pro|
      pro.name = product["title"]
      pro.description = product["description"]
      pro.price = product["price"]
      pro.quantity = rand(1..10)
      downloaded_image = URI.parse(product["image"]).open
      pro.images.attach(io: downloaded_image, filename: "pro_img_#{rand(100..101)}")
      pro.save!
    end
  end
end
require "rails_helper"

RSpec.describe ProductsController, type: :controller do
  let(:category) { FactoryBot.create(:category) }
  let!(:product) { FactoryBot.create(:product, category: category) }

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:ok)
    end
    it "returns http success for particular category products" do
      get :index, params: { category: category.id }
      expect(response).to have_http_status(:ok)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      get :show, params: { id: product.id }
      expect(response).to have_http_status(:ok)
    end

    it 'assigns reviews ordered by latest when "review" param is "latest"' do
      review1 = FactoryBot.create(:review, product: product, created_at: 2.days.ago)
      review2 = FactoryBot.create(:review, product: product, created_at: 1.day.ago)

      get :show, params: { id: product.id, review: 'latest' }

      expect(assigns(:reviews)).to eq([review2, review1])
    end

    it 'assigns reviews ordered by rating ascending when "review" param is "low_to_high"' do
      review1 = FactoryBot.create(:review, product: product, rating: 3)
      review2 = FactoryBot.create(:review, product: product, rating: 5)

      get :show, params: { id: product.id, review: 'low_to_high' }

      expect(assigns(:reviews)).to eq([review1, review2])
    end

    it 'assigns reviews ordered by rating descending when "review" param is "high_to_low"' do
      review1 = FactoryBot.create(:review, product: product, rating: 3)
      review2 = FactoryBot.create(:review, product: product, rating: 5)

      get :show, params: { id: product.id, review: 'high_to_low' }

      expect(assigns(:reviews)).to eq([review2, review1])
    end

    it 'assigns all reviews when "review" param is not specified' do
      review1 = FactoryBot.create(:review, product: product)
      review2 = FactoryBot.create(:review, product: product)

      get :show, params: { id: product.id }

      expect(assigns(:reviews)).to contain_exactly(review1, review2)
    end
  end

end
require "rails_helper"

RSpec.describe ReviewsController, type: :controller do
  let!(:user) { FactoryBot.create(:user) }
  let!(:category) { FactoryBot.create(:category) }
  let!(:product) { FactoryBot.create(:product, category: category) }

  let(:valid_attributes) { { title: "Test", description: "nice shirt", rating: 5, user_id: user.id, product_id: product.id } }
  let(:invalid_attributes) { { title: nil, description: nil, rating: nil, user_id: user.id, product_id: product.id } }
  let(:review) { create(:review, product_id: product.id, user_id: user.id) }

  describe "GET #new" do
    before do
      sign_in user
    end

    describe "GET #new" do
      it "assign a new review" do
        get :new, params: { product_id: product.id }, format: :js
        expect(assigns(:review)).to be_a_new(Review)
      end
    end

    describe "POST #create" do
      it "save a new review with valid_attributes" do
        expect {
          post :create, params: { product_id: product.id, review: valid_attributes }, format: :js
        }.to change(Review, :count).by(1)
      end

      it "save a new review with invalid_attributes" do
        expect {
          post :create, params: { product_id: product.id, review: invalid_attributes }, format: :js
        }.not_to change(Review, :count)
      end
    end

    describe "GET #edit" do
      it "edit a review" do
        get :edit, params: { product_id: product.id, id: review.id }, format: :js
        expect(assigns(:review)).to eq(review)
      end
    end

    describe "PATCH #update" do
      it 'updates the review with valid value' do
        new_rating = 2
        patch :update, params: { product_id: product.id, id: review.id, review: { rating: new_rating } }, format: :js
        review.reload
        expect(review.rating).to eq(new_rating)
      end

      it 'updates the review with invalid value' do
        new_rating = nil
        old_rating = review.rating
        patch :update, params: { product_id: product.id, id: review.id, review: { rating: new_rating } }, format: :js
        review.reload
        expect(review.rating).to eq(old_rating)
      end
    end

    describe "DELETE #destroy" do
      it 'destroys the review' do
        review_temp = FactoryBot.create(:review, product_id: product.id, user_id: user.id)
        expect {
          delete :destroy, params: { product_id: product.id, id: review_temp.id }, format: :js
        }.to change(Review, :count).by(-1)
      end
    end

  end
end
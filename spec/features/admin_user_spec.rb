require 'rails_helper'

RSpec.feature 'AdminUser Dashboard', type: :feature do
  let!(:admin_user) { FactoryBot.create(:admin_user) }

  before do
    login_as(admin_user, scope: :admin_user)
    visit admin_admin_users_path
  end

  scenario 'searching for an admin user by email' do
    fill_in 'q_email', with: 'admin@example.com'
    click_button 'Filter'

    expect(page).to have_content('admin@example.com')
  end

  scenario 'searching for a non-existent admin user' do
    fill_in 'q_email', with: 'nonexistent@example.com'
    click_button 'Filter'

    expect(page).to have_content('No Admin Users found')
  end
end

require 'rails_helper'

describe "users forgot password",type: :system do
  before do
    @user = FactoryBot.create(:user)
    visit new_user_password_path
  end
  it "send password reset instruction for existing user" do
    fill_in "user_email", with: @user.email
    click_button "Send me reset password instructions"

    expect(page).to have_content("You will receive an email with instructions on how to reset your password in a few minutes.")
  end

  it "send password reset instruction for unregister user" do
    fill_in "user_email", with: Faker::Internet.email
    click_button "Send me reset password instructions"

    expect(page).to have_content("not found")
  end

end
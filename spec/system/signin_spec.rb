require 'rails_helper'

describe "Users sign in", type: :system do
  before do
    @user = FactoryBot.create(:user)
    visit new_user_session_path
  end

  it "login with correct email and password" do
    fill_in "user_email", with: @user.email
    fill_in "user_password", with: @user.password
    click_button "Log in"

    expect(page).to have_text "Welcome #{@user.name}"
    expect(page).to have_link "Sign out"
    expect(page).to have_current_path root_path
  end

  it "login with unregister email and password" do
    fill_in "user_email", with: Faker::Internet.email
    fill_in "user_password", with: "111111"
    click_button "Log in"

    expect(page).to have_text "Invalid Email or password."
    expect(page).to have_no_link "Sign out"
  end

  it "login with wrong password" do
    fill_in "user_email", with: @user.email
    fill_in "user_password", with: 111111
    click_button "Log in"

    expect(page).to have_text "Invalid Email or password."
    expect(page).to have_no_link "Sign out"
  end

end
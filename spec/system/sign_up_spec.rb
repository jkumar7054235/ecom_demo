require "rails_helper"

describe "User signs up", type: :system do
  let(:email) { Faker::Internet.email }
  let(:password) { Faker::Internet.password(min_length: 8)}
  let(:name) { Faker::Name.name }
  let(:mobile_no) { Faker::Base.numerify('##########') }

  before do
    visit new_user_registration_path
  end

  it "signup with valid data" do
    fill_in "user_email", with: email
    fill_in "user_password", with: password
    fill_in "user_password_confirmation", with: password
    fill_in "user_name", with: name
    fill_in "user_mobile_no", with: mobile_no
    click_button "Sign up"

    expect(page).to have_content("Welcome! You have signed up successfully.")
  end

  it "signup when email is already exist" do
    user = FactoryBot.create(:user)

    fill_in "user_email", with: user.email
    fill_in "user_password", with: password
    fill_in "user_password_confirmation", with: password
    fill_in "user_name", with: name
    fill_in "user_mobile_no", with: mobile_no
    click_button "Sign up"

    expect(page).to have_text "has already been taken"
  end

  it "signup with miss match password" do
    fill_in "user_email", with: email
    fill_in "user_password", with: password
    fill_in "user_password_confirmation", with: 12345
    fill_in "user_name", with: name
    fill_in "user_mobile_no", with: mobile_no
    click_button "Sign up"

    expect(page).to have_content("confirmation doesn't match")
  end

  it "signup without name" do
    fill_in "user_email", with: email
    fill_in "user_password", with: password
    fill_in "user_password_confirmation", with: password
    fill_in "user_mobile_no", with: mobile_no
    click_button "Sign up"

    expect(page).to have_content("can't be blank")
  end

  it "signup without mobile number" do
    fill_in "user_email", with: email
    fill_in "user_password", with: password
    fill_in "user_password_confirmation", with: password
    fill_in "user_name", with: name
    click_button "Sign up"

    expect(page).to have_content("can't be blank")
  end

end
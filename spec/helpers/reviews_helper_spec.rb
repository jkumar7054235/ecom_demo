require 'rails_helper'

RSpec.describe ReviewsHelper, type: :helper do
  describe '#rating_stars' do
    it 'returns the correct HTML content for a 3-star rating' do
      rating = 3
      expected_html = '<span class="star-rating active">★★★☆☆</span>'.html_safe

      result = helper.rating_stars(rating)

      expect(result).to eq(expected_html)
    end

    it 'returns the correct HTML content for a 5-star rating' do
      rating = 5
      expected_html = '<span class="star-rating active">★★★★★</span>'.html_safe

      result = helper.rating_stars(rating)

      expect(result).to eq(expected_html)
    end

    it 'returns the correct HTML content for a 0-star rating' do
      rating = 0
      expected_html = '<span class="star-rating active">☆☆☆☆☆</span>'.html_safe

      result = helper.rating_stars(rating)

      expect(result).to eq(expected_html)
    end
  end
end

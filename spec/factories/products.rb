FactoryBot.define do
  factory :product do
    name { Faker::Commerce.product_name }
    description { Faker::Lorem.paragraph }
    images { [Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/sample_image.jpg'), 'image/jpeg')] }
    quantity { Faker::Number.between(from: 1, to: 100) }
    price { Faker::Commerce.price(range: 0.99..999.99) }
    association :category, factory: :category

  end
end

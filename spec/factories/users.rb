FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { Faker::Internet.password(min_length: 6) }
    name { Faker::Name.name }
    mobile_no { Faker::Base.numerify('##########') }
  end
end

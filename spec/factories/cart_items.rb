FactoryBot.define do
  factory :cart_item do
    quantity { 1 }
    price { "9.99" }
    association :cart, factory: :cart
    association :product, factory: :product
  end
end

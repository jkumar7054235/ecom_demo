FactoryBot.define do
  factory :order do
    shipping_address { Faker::Address.full_address }
    total_amount { Faker::Commerce.price(range: 1.0..1000.0) }
    association :user, factory: :user
  end
end

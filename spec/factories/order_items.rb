FactoryBot.define do
  factory :order_item do
    quantity { 1 }
    association :order, factory: :order
    association :product, factory: :product
  end
end

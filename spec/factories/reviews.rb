FactoryBot.define do
  factory :review do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    rating { Faker::Number.between(from: 1, to: 5) }
    association :user, factory: :user
    association :product, factory: :product
  end
end

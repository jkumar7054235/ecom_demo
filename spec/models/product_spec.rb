require 'rails_helper'

RSpec.describe Product, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:category) }
    it { should validate_presence_of(:images) }
    it { should validate_presence_of(:quantity) }
    it { should validate_presence_of(:price) }
  end

end

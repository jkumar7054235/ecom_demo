require 'rails_helper'

RSpec.describe Review, type: :model do
  describe "associations" do
    it { should belong_to(:user) }
    it { should belong_to(:product) }
  end

  describe "validations" do
    it { should validate_presence_of(:title)}
    it { should validate_presence_of(:description)}
    it { should validate_presence_of(:rating)}
    it "review with 0 star" do
      review = FactoryBot.build(:review, rating: 0)
      expect(review.save).to be_falsy
    end
  end
end

require 'rails_helper'

RSpec.describe User, type: :model do

  describe 'associations' do
    it { should have_one(:cart) }
    it { should have_many(:orders) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:password) }
    it { should validate_presence_of(:mobile_no) }
  end

end

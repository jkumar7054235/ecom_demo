class User < ApplicationRecord

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one :cart
  has_many :orders
  has_many :reviews

  validates :name, presence: true, format: { with: /([\w\-\']{2,})([\s]+)([\w\-\']{2,})/ }
  validates :mobile_no, presence: true, format: { with: /\A\d{4}-?\d{6}\z/ }

end

class Product < ApplicationRecord

  has_many :order_items
  has_many :cart_items
  belongs_to :category
  has_many :reviews

  has_many_attached :images

  validates :name, presence: true, uniqueness: true
  validates :description, presence: true
  validates :category, presence: true
  validates :images, presence: true
  validates :quantity, presence: true
  validates :price, presence: true

  def self.ransackable_attributes(auth_object = nil)
    ["category_id", "created_at", "description", "id", "name", "price", "quantity", "updated_at"]
  end

  def self.ransackable_associations(auth_object = nil)
    ["cart_items", "category", "order_items"]
  end

end

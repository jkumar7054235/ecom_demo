class Order < ApplicationRecord

  belongs_to :user
  has_many :order_items

  validates :shipping_address, presence: true
  validates :total_amount, presence: true

end

class Review < ApplicationRecord
  belongs_to :user
  belongs_to :product

  validates :title, presence: true
  validates :description, presence: true
  validates :rating, presence: true
  validate :rating_greater_than_one

  def rating_greater_than_one
    if rating.present? && rating < 1
      errors.add(:rating, "atLeast give 1 star")
    end
  end
end

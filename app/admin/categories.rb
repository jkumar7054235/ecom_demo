ActiveAdmin.register Category do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment

  # permit_params :name
  #
  # or
  # actions :index, :edit, :update, :create, :destroy

  permit_params :name
  
end

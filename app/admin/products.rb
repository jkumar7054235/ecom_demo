ActiveAdmin.register Product do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :description, :quantity, :price, :category_id, images: []
  remove_filter :images_attachments, :images_blobs, :order_items, :cart_items


  member_action :delete_product_image, method: :delete do
    product = Product.find(params[:id])
    product.images.find(params[:image_id]).purge_later
    redirect_back(fallback_location: edit_admin_product_path)
  end

  form do |f|
    f.inputs 'Product Details' do
      f.input :category, as: :select, collection: Category.all, prompt: 'Select a Category'
      f.input :name
      f.input :description
      f.input :price
      f.input :quantity

      f.input :images, as: :file, input_html: { multiple: true }
      f.object.images.each do |image|
        span image_tag(image, height: 240)
        span link_to 'Remove',
                     delete_product_image_admin_product_path(id: f.object,image_id: image),
                     method: :delete,
                     data: { confirm: 'Are you sure?' }
      end
    end
    f.actions
  end
  show do
    attributes_table do
      row :name
      row :description
      row :quantity
      row :price
      row :category

      # Display images (assuming 'images' is the attachment name)
      row :images do |product|
        product.images.each do |image|
          span do
            image_tag(image, style: 'max-width: 200px; max-height: 200px;')
          end
        end
      end
    end
  end

  index do
    selectable_column
    id_column
    column :name
    column :description
    column :quantity
    column :price
    column :category

    # Custom column for images
    column 'Images' do |product|
      div class: 'admin-images' do
        product.images.each do |image|
          span do
            image_tag(image, style: 'max-width: 100px; max-height: 100px;')
          end
        end
      end
    end

    actions
  end
  # or
  #
  # permit_params do
  #   permitted = [:name, :description, :quantity, :price, :category_id, :images]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end

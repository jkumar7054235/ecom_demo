module ReviewsHelper
  def rating_stars(rating)
    full_stars = '★' * rating
    empty_stars = '☆' * (5 - rating)
    content = "#{full_stars}#{empty_stars}"

    content_tag(:span, content.html_safe, class: 'star-rating active')
  end
end

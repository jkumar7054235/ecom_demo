class ReviewsController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token if Rails.env.test?

  def new
    @product = Product.find_by_id(params[:product_id])
    @review = @product.reviews.new
    respond_to do |format|
      format.js { }
    end
  end

  def create
    @product = Product.find_by_id(params[:product_id])
    @review = @product.reviews.new(review_params)
    if @review.save
      flash[:notice] = "Review was successfully created."
      redirect_to @product
    else
      flash[:error] = @review.errors
      redirect_back(fallback_location: product_path(@product))
    end
  end

  def edit
    @product = Product.find_by_id(params[:product_id])
    @review = @product.reviews.find(params[:id])
    respond_to do |format|
      format.js { }
    end
  end

  def update
    @product = Product.find_by_id(params[:product_id])
    @review = @product.reviews.find(params[:id])
    if @review.update(review_params)
      flash[:notice] = "Review was successfully updated."
      redirect_to @product
    else
      flash[:error] = @review.errors
      redirect_back(fallback_location: product_path(@product))
    end
  end

  def destroy
    @product = Product.find_by_id(params[:product_id])
    @review = @product.reviews.find(params[:id])
    @review.destroy
    flash[:notice] = "Review was successfully deleted."
    redirect_to @product
  end

  private
  def review_params
    params.require(:review).permit(:title, :description, :rating, :user_id)
  end
end

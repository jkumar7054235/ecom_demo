class ProductsController < ApplicationController
  def index
    @categories = Category.all.includes(:products)
    @products = []
    if params[:category].present?
      @products = Category.find_by_id(params[:category])&.products
    else
      @products = Product.all
    end
  end

  def show
    @product = Product.find(params[:id])
    @reviews = case params[:review]
               when 'latest'
                 @product.reviews.joins(:user).select('reviews.*, users.name AS review_by').order(created_at: :desc)
               when 'low_to_high'
                 @product.reviews.joins(:user).select('reviews.*, users.name AS review_by').order(rating: :asc)
               when 'high_to_low'
                 @product.reviews.joins(:user).select('reviews.*, users.name AS review_by').order(rating: :desc)
               else
                 @product.reviews.joins(:user).select('reviews.*, users.name AS review_by')
               end
    @review = @product.reviews.new
  end
end
